FROM alpine:3.8
LABEL maintainer="Vagner Praia <vagnerpraia@gmail.com>"

RUN mkdir /app

#ENV ACCESS_TOKEN abc
#ENV SECRET_TOKEN xyz

COPY ./bin/gnatsd /app/gnatsd
COPY ./conf/gnatsd.conf /app/gnatsd.conf

EXPOSE 4222 8222 6222

ENTRYPOINT ["/app/gnatsd"]
CMD ["-c", "gnatsd.conf"]